export const environment = {
  production: true,
  base_url: 'http://35.240.202.3',
  api_url: 'http://35.240.202.3/apis/',
//  api_url: 'http://localhost:8080/apis/',
  back_end_less_url: 'https://backendlessappcontent.com/'
};
