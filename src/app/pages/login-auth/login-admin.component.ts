import {Component, ElementRef, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {ApiService} from '../../services/api.service';
import {SidebarService} from '../../services/sidebar.service';
import {UserService} from '../../services/user.service';
import {LocalStorageServices} from '../../services/local-storage.services';
import {LOCAL_STORAGE} from '../../model/LOCAL_STORAGE';
import Swal from "sweetalert2";

declare var $: any;

@Component({
    selector: 'app-login-cmp',
    templateUrl: './login-admin.component.html'
})

export class LoginAdminComponent implements OnInit, OnDestroy {
    test: Date = new Date();
    private toggleButton: any;
    private sidebarVisible: boolean;
    private nativeElement: Node;
    loginForm: FormGroup;
    invalidLogin = false;
    localStorageService: LocalStorageServices;

    constructor(private formBuilder: FormBuilder, private router: Router, private apiService: ApiService, private element: ElementRef,
                private sidebarService: SidebarService, private userService: UserService) {
        this.nativeElement = element.nativeElement;
        this.sidebarVisible = false;
        this.localStorageService = new LocalStorageServices();
    }

    async ngOnInit() {
        console.log('On init')

        this.loginForm = this.formBuilder.group({
            username: ['', Validators.compose([Validators.required])],
            password: ['', Validators.required]
        });

        const navbar: HTMLElement = this.element.nativeElement;

        this.toggleButton = navbar.getElementsByClassName('navbar-toggle')[0];
        const body = document.getElementsByTagName('body')[0];
        body.classList.add('login-page');
        body.classList.add('off-canvas-sidebar');
        const card = document.getElementsByClassName('card')[0];
        setTimeout(function () {
            // after 1000 ms we add the class animated to the login/register card
            card.classList.remove('card-hidden');
        }, 700);


    }

    async ngAfterViewInit() {
        const token = this.localStorageService.getValue(LOCAL_STORAGE.TOKEN)
        const checkStatus = await this.checkToken(token);
        if (checkStatus) {
            this.router.navigate(['admin/dashboard']);

        } else {
            this.localStorageService.clearLocalStorage(LOCAL_STORAGE.TOKEN);
            this.localStorageService.clearLocalStorage(LOCAL_STORAGE.USER_ID);
            this.localStorageService.clearLocalStorage(LOCAL_STORAGE.USER_NAME);

        }

    }

    private async checkToken(token) {
        if (token == null) {
            return false;
        }
        let user_name = null;
        const promise = [];
        const username_local = this.localStorageService.getValue(LOCAL_STORAGE.USER_NAME);
        if (username_local == null) {
            return false;
        }
        promise.push(this.userService.exeGetCurrentUser(username_local).then(data => {
            user_name = data.username;
        }).catch(err => {
            console.log(err)
        }));
        await Promise.all(promise);
        if (user_name != null && user_name.toString().length > 0) {
            return true;
        }
    }


    sidebarToggle() {
        const toggleButton = this.toggleButton;
        const body = document.getElementsByTagName('body')[0];
        const sidebar = document.getElementsByClassName('navbar-collapse')[0];
        if (this.sidebarVisible === false) {
            setTimeout(function () {
                toggleButton.classList.add('toggled');
            }, 500);
            body.classList.add('nav-open');
            this.sidebarVisible = true;
        } else {
            this.toggleButton.classList.remove('toggled');
            this.sidebarVisible = false;
            body.classList.remove('nav-open');
        }
    }

    ngOnDestroy() {
        const body = document.getElementsByTagName('body')[0];
        body.classList.remove('login-page');
        body.classList.remove('off-canvas-sidebar');
    }

    onSubmit() {
        if (this.loginForm.invalid) {
            return;
        }

        const loginPayload = {
            username: this.loginForm.controls.username.value,
            password: this.loginForm.controls.password.value
        };
        this.apiService.login(loginPayload).subscribe(data => {
            if (data.status === 200) {
                this.localStorageService.saveLocalStorage(LOCAL_STORAGE.USER_NAME, data.result.username);
                this.localStorageService.saveLocalStorage(LOCAL_STORAGE.TOKEN, data.result.token);

                this.userService.getCurrentUser(data.result.username).subscribe(user => {
                    this.localStorageService.saveLocalStorage(LOCAL_STORAGE.USER_ID, user.id);
                    if (user.rule === 0) {
                        this.sidebarService.useHRRoutes()
                    } else if (user.rule === 1) {
                        this.sidebarService.useInterviewRoutes()
                    }
                    Swal.fire({
                        position: 'top-end',
                        type: 'success',
                        title: 'Login successfully!',
                        showConfirmButton: false,
                        timer: 1000
                    }).then(() => {
                        this.router.navigate(['admin/dashboard']);

                    })


                })
            } else {
                this.invalidLogin = true;
                alert(data.message);
            }
        },error=>{
            Swal.fire({
                type: 'error',
                title: 'Oops...',
                showConfirmButton: false,
                text: 'Email or password is incorrect',
            });
        });
    }
}
