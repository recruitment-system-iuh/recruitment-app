import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PagesRoutes } from './pages.routing';

import { RegisterComponent } from './register/register.component';
import { LoginGuestComponent } from './login-guest/login-guest.component';
import {LoginAdminComponent} from './login-auth/login-admin.component';
import {AuthLayoutAdminComponent} from '../layouts/auth-layout-admin/auth-layout-admin.component';
import {AuthLayoutGuestComponent} from '../layouts/auth-layout-guest/auth-layout-guest.component';
import {ComponentsModule} from '../components/components.module';
import {MaterialModule} from '../app.module';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(PagesRoutes),
    FormsModule,
    ReactiveFormsModule,
    ComponentsModule,
    MaterialModule
  ],
  declarations: [
    LoginGuestComponent,
    RegisterComponent,
    LoginAdminComponent,
    AuthLayoutAdminComponent,
    AuthLayoutGuestComponent
  ]
})

export class PagesModule {}
