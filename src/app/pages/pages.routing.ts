import { Routes } from '@angular/router';

import { RegisterComponent } from './register/register.component';
import { LoginGuestComponent } from './login-guest/login-guest.component';
import {LoginAdminComponent} from './login-auth/login-admin.component';
import {AuthLayoutAdminComponent} from '../layouts/auth-layout-admin/auth-layout-admin.component';
import {AuthLayoutGuestComponent} from '../layouts/auth-layout-guest/auth-layout-guest.component';
export const PagesRoutes: Routes = [
    {
        path: '',
        redirectTo: 'login-admin',
        pathMatch: 'full',
    },
    {
        path: 'login-admin',
        component: LoginAdminComponent
    }, {
        path: '',
        component: AuthLayoutGuestComponent,
        children: [ {
            path: 'login-guest',
            component: LoginGuestComponent
        }, {
            path: 'register',
            component: RegisterComponent
        }]
    }
];
