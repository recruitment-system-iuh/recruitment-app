import {Component, OnInit, OnDestroy} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {LocalStorageServices} from '../../services/local-storage.services';
import {UserService} from '../../services/user.service';
import {Router} from "@angular/router";
import Swal from "sweetalert2";


@Component({
    selector: 'app-register-cmp',
    templateUrl: './register.component.html'
})

export class RegisterComponent implements OnInit, OnDestroy {
    test: Date = new Date();
    private toggleButton: any;
    private sidebarVisible: boolean;
    private nativeElement: Node;
    loginForm: FormGroup;
    invalidLogin = false;
    localStorageService: LocalStorageServices;
    genderList = ['Male', 'Female'];


    constructor(private formBuilder: FormBuilder, private userService: UserService,private router: Router) {
        this.sidebarVisible = false;
        this.localStorageService = new LocalStorageServices();
    }

    ngOnInit() {
        const body = document.getElementsByTagName('body')[0];
        body.classList.add('register-page');
        body.classList.add('off-canvas-sidebar');
        this.loginForm = this.formBuilder.group({
            email: ['', Validators.compose([Validators.required])],
            password: ['', Validators.required],
            confirmPass: ['', Validators.required],
            firstName: ['', Validators.required],
            lastName: ['', Validators.required],
            address: [''],
            check: ['true', Validators.pattern('true')]

        }, {validator: this.checkPasswords});


    }

    checkPasswords(group: FormGroup) { // here we have the 'passwords' group
        let pass = group.controls.password.value;
        let confirmPass = group.controls.confirmPass.value;

        return pass === confirmPass ? null : {notSame: true}
    }

    ngOnDestroy() {
        const body = document.getElementsByTagName('body')[0];
        body.classList.remove('register-page');
        body.classList.remove('off-canvas-sidebar');
    }

    onSubmit() {
        let new_user = {

            username: "",
            password: "",
            rule: "",
            person: {
                address: "",
                birthday: "",
                firstName: "",
                lastName: "",
                gender: "",
                email: "",
                phoneNumber: ""
            }
        };


        new_user.username = this.loginForm.value.email;

        new_user.rule = '3';
        new_user.person.address = this.loginForm.value.address;
        new_user.person.birthday = '';
        new_user.person.firstName = this.loginForm.value.firstName;
        new_user.person.lastName = this.loginForm.value.lastName;
        new_user.person.gender = '';
        new_user.person.email = this.loginForm.value.email;
        new_user.person.phoneNumber = '';
        new_user.password=this.loginForm.value.password;
        console.log("New")
        console.log(new_user)



        this.userService.postUser(new_user).then(data => {

            if(data.status ==200){
                Swal.fire({
                    position: 'top-end',
                    type: 'success',
                    title: 'Your account created!',
                    showConfirmButton: false,
                    timer: 2000
                }).then(()=>{
                    this.router.navigate(['auth/login-guest']);

                })

            }else{
                Swal.fire({
                    type: 'error',
                    title: 'Oops...',
                    text: 'Something went wrong!<br>Please try again',

                })
            }




        }).catch(error => {
            console.log('Error')
            console.log(error)
        })


    }
}
