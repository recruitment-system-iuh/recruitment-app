import {Component, OnInit} from '@angular/core';
import {UserService} from './services/user.service';
import {SidebarService} from './services/sidebar.service';
import {LocalStorageServices} from './services/local-storage.services';
import {LOCAL_STORAGE} from './model/LOCAL_STORAGE';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  constructor(private  userService: UserService,
              private sidebarService: SidebarService) {

  }

  ngOnInit(): void {
    const localStorageService = new LocalStorageServices();
    const user_name = localStorageService.getValue(LOCAL_STORAGE.USER_NAME);
    if (user_name) {
    this.userService.getCurrentUser(user_name).subscribe( user => {
      if (!user) {
        return;
      }
      console.log(user.rule);
      if (user.rule === 0) {
        this.sidebarService.useHRRoutes();
      } else if (user.rule === 1) {
        this.sidebarService.useInterviewRoutes();
      }
    })
    }
  }
}
