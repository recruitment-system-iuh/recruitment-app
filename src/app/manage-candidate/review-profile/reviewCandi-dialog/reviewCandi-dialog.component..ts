import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {DataReview} from '../../../model/dataReview';
import {JobcandidateService} from '../../../services/jobcandidate.service';
import * as _ from 'lodash'

declare const $: any;

@Component({
  selector: 'app-review-detail',
  templateUrl: './reviewCandi-dialog.component.html',
  styleUrls: ['./reviewCandi-dialog.component..scss']
})
export class ReviewCandiDialogComponent implements OnInit {

  dataReview: DataReview;
  jobDescription: [];
  jobRecruitment: [];
  jobConfigDetails: ConfigDetail[];
  resumeDetails: ConfigDetail[];
  constructor(public dialogRef: MatDialogRef<ReviewCandiDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private jobcandService: JobcandidateService) {
    console.log('data');
    console.log(data);
  }

  ngOnInit(): void {
    this.jobcandService.getDateReview(this.data.jobid, this.data.candid).subscribe( data => {
      console.log(data);
      this.dataReview = data;
      // @ts-ignore
      this.jobDescription = data.jobDescription.match( /[^\.!\?]+[\.!\?]+/g );
      // @ts-ignore
      this.jobRecruitment = data.jobRequirement.match( /[^\.!\?]+[\.!\?]+/g );
      this.jobConfigDetails = this.convertConfigDetail(data.jobConfigs, 'configTypeName');
      console.log(this.jobConfigDetails );
      for ( let i = 0; i < this.jobConfigDetails.length ; i++) {
        const jobconfigDetail = this.jobConfigDetails[i];
        for ( let j = 0; j < jobconfigDetail.detail.length ; j++) {
          // @ts-ignore
          this.jobConfigDetails[i].detail[j].content = this.parseJsonToContent(jobconfigDetail.detail[j].content);
        }
      }
      this.resumeDetails = this.convertConfigDetail(data.resumeDetais, 'header');
      for ( let i = 0; i < this.resumeDetails.length ; i++) {
        const resumeDetail = this.resumeDetails[i];
        for ( let j = 0; j < resumeDetail.detail.length ; j++) {
          // @ts-ignore
          this.resumeDetails[i].detail[j].content = this.parseJsonToContent(resumeDetail.detail[j].content);
        }
      }
      console.log(this.resumeDetails );
    })
  }
  parseJsonToContent(content) {
    const contentJson = JSON.parse(content);
    const listKey = Object.keys(contentJson);
    const listObj = listKey.map(item => {
      if (contentJson[item.toString()]) {
        return {
          key: item,
          value: contentJson[item.toString()]
        }
      }
    })
    return listObj;
  }
  convertConfigDetail(listConfigs, fillterName) {
    console.log(listConfigs)
    // Nhóm các JobConfig có cùng configType lại
    // @ts-ignore
    const result: ConfigDetail[] = _.chain(listConfigs)
        .groupBy(fillterName)
        .toPairs().map( (currentItem) => {
          return _.zipObject([fillterName, 'detail'], currentItem); })
        .value();
    return result;
  }
  passReview() {
    const result = {
      'status': 1,
    }
    this.dialogRef.close(result);
  }
  falseReview() {
    const result = {
      'status': 0,
    }
    this.dialogRef.close(result);
  }
}
export interface ConfigDetail {
  configTypeName: String; // configTypeName or Header
  detail: [{
  }]
}
