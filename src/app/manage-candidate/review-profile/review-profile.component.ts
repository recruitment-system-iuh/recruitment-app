import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatPaginator} from '@angular/material';
import {DataSource, SelectionModel} from '@angular/cdk/collections';
import {JobcandidateService} from '../../services/jobcandidate.service';
import {Observable} from 'rxjs';
import {JobCandi} from '../../model/JobCandi';
import * as moment from 'moment';
import {ReviewCandiDialogComponent} from './reviewCandi-dialog/reviewCandi-dialog.component.';
import swal from 'sweetalert2';

@Component({
    selector: 'app-review-profile',
    templateUrl: './review-profile.component.html',
    styleUrls: ['./review-profile.component.scss']
})
export class ReviewProfileComponent implements OnInit, AfterViewInit {
    @ViewChild(MatPaginator) paginator: MatPaginator;
    selection = new SelectionModel<JobCandi>(true, []);
    displayedColumns: string[] = ['selected', 'name', 'position', 'jobName', 'date', 'action'];
    dataSource;
    touch: boolean;
    skills = [
        {value: 'java', viewValue: 'Java Senior'},
        {value: 'c#', viewValue: 'C# Junior'},
        {value: 'Spring framework', viewValue: 'Spring framework'},
        {value: 'PHP', viewValue: 'PHP'},

    ];
    sources = [
        {value: 'java', viewValue: 'Itviec'},
        {value: 'c#', viewValue: 'VietnamWork'},
        {value: 'Spring framework', viewValue: 'TopCV'},
        {value: 'PHP', viewValue: 'Carrerbuilder'},

    ];
    currentSkill: string [];
    currentSource: any;
    picker1: any;

    constructor(private jobcandService: JobcandidateService,
                public dialog: MatDialog) {
    }

    ngOnInit() {
        this.dataSource = new JobCandiDataSource(this.jobcandService).connect();
        this.dataSource.paginator = this.paginator;
        // console.log(this.listjobcandidate[0]);
    }

    ngAfterViewInit(): void {
        //  console.log(this.listjobcandidate[0]);
    }

    onCandidateClick(row) {
        console.log(row);
    }

    formatDate(data) {
        data = moment(data).format('MMMM DD YYYY, h:mm:ss a');
        return data;
    }

    isAllSelected() {
        const numSelected = this.selection.selected.length;
        this.dataSource.subscribe(data => {
            console.log(data);
        })
        const numRows = this.dataSource.data.length;
        return numSelected === numRows;
    }

    /** Selects all rows if they are not all selected; otherwise clear selection. */
    masterToggle() {
        this.isAllSelected() ?
            this.selection.clear() :
            this.dataSource.data.forEach(row => this.selection.select(row));
    }

    reviewCandidate(jobid, candid, jobcanID) {
        console.log(jobid + ' --- ' + candid);
        const dialogRef = this.dialog.open(ReviewCandiDialogComponent,
            {
                disableClose: true,
                data: {jobid: jobid, candid: candid},
                height: '90%',
                width: '95%',
                panelClass: 'feedback-dialog'
            });
        dialogRef.afterClosed().subscribe(result => {
            // console.log(`Dialog result: ${result}`);
            console.log(jobcanID)
            if (result !== '') {

                if (result.status === 0) {
                    // false review
                    swal.fire({
                        type: 'warning',
                        title: 'False Review!...',
                        text: 'You clicked the button!',
                    })
                } else {
                    // Pass review
                    this.jobcandService.updateStatusViewed(jobcanID, '').subscribe(response => {
                        console.log(response);
                        swal.fire({
                            type: 'success',
                            title: 'Pass reivew!...',
                            text: 'You clicked the button!',
                        })
                        window.location.reload();
                    })
                }
            } else {
                swal.fire({
                    type: 'warning',
                    title: 'Cancel!...',
                    text: 'You clicked the button!',
                })
            }
        });
    }
}
export class JobCandiDataSource extends DataSource<JobCandi> {
    constructor(private jobcandidateService: JobcandidateService) {
        super();
    }
    connect(): Observable<JobCandi[]> {
        return this.jobcandidateService.getJobCandidateByStatus('waited');
    }

    disconnect() {
    };
}
