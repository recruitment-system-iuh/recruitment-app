import {Component, OnInit} from '@angular/core';

@Component({
    selector: 'app-import-candidate',
    templateUrl: './import-candidate.component.html',
    styleUrls: ['./import-candidate.component.scss']
})
export class ImportCandidateComponent implements OnInit {
    simpleSlider = 0;
    doubleSlider = [20, 60];

    regularItems = ['Pizza', 'Pasta', 'Parmesan'];
    touch: boolean;

    selectedValue: string;
    currentCity: string[];

    selectTheme = 'primary';
    cities = [
        {value: 'paris-0', viewValue: 'Male'},
        {value: 'miami-1', viewValue: 'Female'},

    ];
    skills = [
        {value: 'java', viewValue: 'Java'},
        {value: 'c#', viewValue: 'C#'},
        {value: 'Spring framework', viewValue: 'Spring framework'},
        {value: 'PHP', viewValue: 'PHP'},

    ];
    currentSkill: string [];
    fileNotes: string [];
    constructor() {
    }

    ngOnInit() {
    }

    hihi() {
        this.fileNotes.forEach((data) => {
            console.log(data);
        })
    }
}
