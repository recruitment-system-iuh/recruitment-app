import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {ImportCandidateComponent} from './import-candidate/import-candidate.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { NouisliderModule } from 'ng2-nouislider';
import { TagInputModule } from 'ngx-chips';
import {ReviewProfileComponent} from './review-profile/review-profile.component';
import {CandidateRoutes} from './manage-candidate.routing';
import {MaterialModule} from '../app.module';
import { CandidateProfileComponent } from './candidate-profile/candidate-profile.component';
import {ReviewCandiDialogComponent} from './review-profile/reviewCandi-dialog/reviewCandi-dialog.component.';
import {FeedbackDialogComponent} from '../interviewer/feedback-dialog/feedback-dialog.component.';
@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(CandidateRoutes),
        FormsModule,
        ReactiveFormsModule,
        NouisliderModule,
        TagInputModule,
        MaterialModule,
    ],
    declarations: [
        ImportCandidateComponent,
        ReviewProfileComponent,
        CandidateProfileComponent,
        ReviewCandiDialogComponent],
    entryComponents: [ReviewCandiDialogComponent]
})
export class ManageCandidateModule {
}
