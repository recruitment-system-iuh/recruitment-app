import { Routes } from '@angular/router';
import {ImportCandidateComponent} from './import-candidate/import-candidate.component';
import {ReviewProfileComponent} from './review-profile/review-profile.component';
import {CandidateProfileComponent} from './candidate-profile/candidate-profile.component';


export const CandidateRoutes: Routes = [

    {
        path: '',
        children: [ {
            path: 'import',
            component: ImportCandidateComponent
        }, {
            path: 'review',
            component: ReviewProfileComponent
        }, {
            path: 'profile',
            component: CandidateProfileComponent
        }
        ]
    }
];
