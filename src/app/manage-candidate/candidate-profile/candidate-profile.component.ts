import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {CandidateService} from '../../services/candidate.service';
import {DomSanitizer} from '@angular/platform-browser';
import {JobcandidateService} from '../../services/jobcandidate.service';
import * as moment from 'moment';
import * as _ from 'lodash'
import swal from 'sweetalert2';
import {CandidateProfile} from '../../model/candidateProfile';
import {JobCandi} from '../../model/JobCandi';
import {LOCAL_STORAGE} from '../../model/LOCAL_STORAGE';

@Component({
  selector: 'app-candidate-profile',
  templateUrl: './candidate-profile.component.html',
  styleUrls: ['./candidate-profile.component.scss']
})

export class CandidateProfileComponent implements OnInit {
    model: any = {};
    touch: boolean;
    genders = [
        {value: 'male', viewValue: 'Male'},
        {value: 'female', viewValue: 'Female'},
    ];
    status = [
        {value: 'wait', viewValue: 'Wait'},
        {value: 'viewed', viewValue: 'Viewed'},
        {value: 'interview', viewValue: 'Interview'},
        {value: 'offer', viewValue: 'Offer'},
        {value: 'probation', viewValue: 'Probation'}
    ];
    candidatProfile: CandidateProfile;
    jobCandi: JobCandi;
    birthday: Date;
    candidateID: string;
    constructor(private router: Router,
                private route: ActivatedRoute,
                private sanitizer: DomSanitizer,
                private candiService: CandidateService,
                private jobcandiService: JobcandidateService) {
        this.sanitizer = sanitizer;
    }

    ngOnInit() {
        const jobcandi = this.route.snapshot.paramMap.get('jobcandID');
        this.candidateID = this.route.snapshot.paramMap.get('candidateID');
        this.candiService.getCandidateByID( this.candidateID).subscribe( candi => {
            console.log(candi);
            this.candidatProfile = candi
            this.birthday =  new Date(candi.person.birthday);
            // @ts-ignore
            this.candidatProfile.logs = candi.logs.sort((a, b) => new Date(b.createDateTime) - new Date(a.createDateTime));
        })
        this.jobcandiService.getJobCandidateByID(jobcandi).subscribe( jobcandidate => {
            console.log(jobcandidate)
            this.jobCandi = jobcandidate;
        })
    }

    getURLCV() {
        if (this.jobCandi !== null) {
        return this.sanitizer.bypassSecurityTrustResourceUrl(this.jobCandi.urlCV);
        }
    }
    formatDate(data) {
        data = moment(data).format('MMMM DD YYYY, h:mm:ss a');
        return data;
    }
    UpdateCandidate(person) {
        console.log(this.model)
        if (_.isEmpty(this.model)) {
            swal.fire({
                type: 'error',
                title: 'Oops...',
                text: 'No field update!',
            })
            return;
        }
        const updateBody: InfoUpdateCandi = {
            user_id: window.localStorage.getItem(LOCAL_STORAGE.USER_ID),
            person: person,
            logDetail: [ {
                changeValue: '',
                toValue: '',
                updateField: ''
            }]
        }
        updateBody.logDetail.pop();
        for (const updateField in this.model) {
            const toValue = this.model[updateField];
            const changevalue = person[updateField];
            person[updateField] = toValue // update value person
            const logDetail = {
                'changeValue': changevalue,
                'toValue': toValue,
                'updateField': updateField
            }
            updateBody.logDetail.push(logDetail);
        }
        updateBody.person = person
        // let inforUpdateCandi: InfoUpdateCandi;
        // if (this.model.first !== undefined) {
        //     inforUpdateCandi.log.updateField = 'firstName';
        // }
        person.birthday =  moment(person.birthday).format('DD/MM/YYYY')
        updateBody.person = person;
        console.log(updateBody);
        this.candiService.updateCandidatebyID( this.candidateID, updateBody).subscribe( response => {
            console.log(response);
            swal.fire({
                type: 'success',
                title: 'Oops...',
                text: 'Update Candidate Successful!',
            })
            window.location.reload()
        })
    }

    alert(aaa: string) {
        alert('aa')
    }
}
export interface InfoUpdateCandi {
    user_id: string;
    person: {
        firstName: string;
        lastName: string;
        birthday: string;
        address: string;
        phoneNumber: string;
        gender: string;
        email: string;
    },
    logDetail: [{
        changeValue: string;
        toValue: string;
        updateField: string;
    }]
}

