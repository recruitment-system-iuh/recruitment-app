import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {JobCandi} from '../model/JobCandi';
import {InterviewJobCandidate} from '../model/interviewJobCandidate';
import {environment} from '../../environments/environment.prod';
import {DataFeedback} from '../model/dataFeedback';
import {DataReview} from '../model/dataReview';

@Injectable({
    providedIn: 'root'
})
export class JobcandidateService {

    constructor(private httpClient: HttpClient) {
    }
    getJobCandidateByStatus(status: String): Observable<JobCandi[]> {
       return this.httpClient.get<JobCandi[]>(`${environment.api_url}` + 'jobcandidates' + `/status/${status}`).pipe(
           map(res => res['result']));
    }
    getJobCandidateByID(id: String): Observable<JobCandi> {
        return this.httpClient.get<(JobCandi)>(`${environment.api_url}`  + 'jobcandidates' + `/${id}`).pipe(
            map(res => res['result']));
    }
    getJobDetailAndCandiInterview(jobid: string): Observable<InterviewJobCandidate[]> {
        return this.httpClient.get<(InterviewJobCandidate[])>(`${environment.api_url}`
            + 'jobcandidates' + `/interview/${jobid}`).pipe(
            map(res => res['result']));
    }
    getDataFeedback(jobcanID: string): Observable<DataFeedback> {
        return this.httpClient.get<(DataFeedback)>(`${environment.api_url}`  + 'jobcandidates' + `/feedback/${jobcanID}`).pipe(
            map(res => res['result']));
    }
    updateFeedback(jobcanID: string, body: any): Observable<Object> {
        return this.httpClient.put(
            `${environment.api_url}` + 'jobcandidates' + `/feedback/${jobcanID}`, body
        );
    }
    getDateReview(jobID: string, candID: string): Observable<DataReview> {
        return this.httpClient.get<(DataReview)>(`${environment.api_url}`  + 'jobcandidates' + `/review/${jobID}/${candID}`).pipe(
            map(res => res['result']));
    }
    updateStatusViewed(jobcanID: string, body: string): Observable<Object> {
        return this.httpClient.put(
            `${environment.api_url}` + 'jobcandidates' + `/review/${jobcanID}`, body
        );
    }
    reportJobCandidate(): Observable<[]> {
        return this.httpClient.get<([])>(`${environment.api_url}`  + 'jobcandidates' + `/report/`).pipe(
            map(res => res['result']));
    }
}
