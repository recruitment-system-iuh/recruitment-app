
export class LocalStorageServices {

    constructor() {

    }

    getValue(key) {
        const value = window.localStorage.getItem(key);
        return value;
    }

    clearLocalStorage(key) {
        window.localStorage.removeItem(key);

    }

    saveLocalStorage(key, value) {

        window.localStorage.setItem(key, value);

    }
}
