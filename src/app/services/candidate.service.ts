import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {CandidateProfile} from '../model/candidateProfile';
import {environment} from '../../environments/environment.prod';

@Injectable({
    providedIn: 'root'
})
export class CandidateService {

    constructor(private httpClient: HttpClient) {
    }
    getCandidateByID(id: String): Observable<CandidateProfile> {
        return this.httpClient.get<CandidateProfile>(`${environment.api_url}` + 'candidates' + `/${id}`).pipe(
             map(res => res['result']));
    }
    updateCandidatebyID(id: string, body: any): Observable<Object> {
        return this.httpClient.put(
            `${environment.api_url}` + 'candidates' + `/${id}` , body
        );
    }
    applyJob(body):Observable<Object> {
        return this.httpClient.post(
            `${environment.api_url}` + 'candidates/applyCV', body
        );
    }
}
