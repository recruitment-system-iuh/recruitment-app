import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {Job} from '../model/job';
import {InterviewJob} from '../model/interviewJob';
import {environment} from '../../environments/environment.prod';
import {Schedule} from '../model/MODEL';

@Injectable({
    providedIn: 'root'
})
export class JobService {

    constructor(private httpClient: HttpClient) {
    }
    getlistjob(): Observable<Job> {
       return this.httpClient.get<Job>(`${environment.api_url}` + 'jobs').pipe(
           map(res => res['result']));
    }
    /*
    * jobid: Biến Optional
    * Nếu không có: Liệt kê các JOb By UserID
    * Ngược lại: Liệt kê các Job by UserID and JobID*/
    getJobByUserID(userid: number, jobid: number): Observable<InterviewJob[]> {
        return this.httpClient.get<(InterviewJob[])>(`${environment.api_url}` + 'jobs' + `/listjob/${userid}/${jobid}`).pipe(
            map(res => res['result']));
    }
    getJobByID(jobid: number): Observable<Job> {
        return this.httpClient.get<(Job)>(`${environment.api_url}` + 'jobs' + `/${jobid}`).pipe(
            map(res => res['result']));
    }
    saveNewJob(data):  Observable<Object> {
        return this.httpClient.post(
            `${environment.api_url}` + 'jobs' , data
        );
    }
}
