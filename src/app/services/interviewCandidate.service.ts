import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {JobcandidateInterview, ScheduleInterviewDetail} from '../model/scheduleInterviewDetail';
import {environment} from '../../environments/environment.prod';
import {Schedule} from '../model/MODEL';

@Injectable({
    providedIn: 'root'
})
export class InterviewCandidateService {

    constructor(private httpClient: HttpClient) {
    }
    getScheduleInterviewDetailbyJobcanID(jobcanID: string): Observable<ScheduleInterviewDetail[]> {
        return this.httpClient.get<ScheduleInterviewDetail[]>(`${environment.api_url}` + 'schedule' + `/${jobcanID}`).pipe(
             map(res => res['result']));
    }
    createSchedule(body: Schedule): Observable<Object> {
        return this.httpClient.post(
            `${environment.api_url}` + 'schedule' , body
        );
    }
    getJobCandidateInterviewByID(id: number) {
        return this.httpClient.get<JobcandidateInterview>(`${environment.api_url}` + 'schedule' + `/getone/${id}`).pipe(
            map(res => res['result']));
    }
    updateQuestionJCI(id: number, body: any): Observable<Object> {
        return this.httpClient.put(
            `${environment.api_url}` + 'schedule' + `/${id}` , body
        );
    }
}
