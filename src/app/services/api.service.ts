import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {ApiResponse} from '../model/api.response';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment.prod';
import {BACK_END_LESS} from '../model/BACKENNLESS';

@Injectable({
    providedIn: 'root'
})
export class ApiService {

    constructor(private httpClient: HttpClient) {
    }

    login(loginPayload): Observable<ApiResponse> {
        return this.httpClient.post<ApiResponse>(`${environment.api_url}` + 'token/generate-token', loginPayload);
    }

    getConfigTypes(): Promise<any> {
        return new Promise((resolve, reject) => {
            this.httpClient.get<ApiResponse>(`${environment.api_url}` + 'configtypes').subscribe(data => {
                resolve(data);
            }, error => {
                reject(error);
            })
        });
    }

    getListJobConfigById(id: String): Promise<any> {
        return new Promise((resolve, reject) => {
            this.httpClient.get<ApiResponse>(`${environment.api_url}` + 'configtypes' + `/${id}`).subscribe(data => {
                resolve(data);
            }, error => {
                reject(error);
            })
        });
    }

    getListJob(): Promise<any> {
        return new Promise((resolve, reject) => {
            this.httpClient.get<ApiResponse>(`${environment.api_url}` + 'jobs').subscribe(data => {
                resolve(data);
            }, error => {
                reject(error);
            })
        });
    }

    saveNewJob(data): Promise<any> {
        return new Promise((resolve, reject) => {
            this.httpClient.post(`${environment.api_url}` + 'jobs', JSON.stringify(data)).subscribe(job => {
                resolve(job);
            }, error => {
                reject(error);
            })
        });
    }

    saveFile(file,file_name): Promise<any> {

        let formData: FormData = new FormData();
        formData.append('upload', file, file.name);
        return new Promise((resolve, reject) => {
            this.httpClient.post(`${environment.back_end_less_url}${BACK_END_LESS.APPLICATION_ID}/${BACK_END_LESS.REST_API_KEY}/${BACK_END_LESS.FILES}${BACK_END_LESS.PATH_STORAGE}${file_name}?overwrite=true`, formData).subscribe(data => {
                resolve(data);
            }, error => {
                reject(error);
            })
        });
    }

    public get(path: string, options?, params?: HttpParams): Observable<any> {
        return this.httpClient.get(
            `${environment.api_url}` + path,
            {
                params: params,
                withCredentials: true,
                observe: 'response'
            }
        )
    }

}

