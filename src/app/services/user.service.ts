import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {User} from '../model/user';
import {environment} from '../../environments/environment.prod';
import {LocalStorageServices} from './local-storage.services';

@Injectable({
    providedIn: 'root'
})
export class UserService {
    localStorageServices: LocalStorageServices;

    constructor(private httpClient: HttpClient) {
        this.localStorageServices = new LocalStorageServices();

    }

    getCurrentUser(username): Observable<User> {
        return this.httpClient.get<User>(`${environment.api_url}users/username/${username}`).pipe(
            map(res => res['result']));

    }

    postUser(user): Promise<any> {
        return new Promise((resolve, reject) => {
            this.httpClient.post(`${environment.api_url}users`, user).subscribe(data => {
                resolve(data);
            }, error => {
                reject(error);
            })

        })
    }


    exeGetCurrentUser(username): Promise<any> {
        return new Promise(((resolve, reject) => {
                this.getCurrentUser(username).subscribe(value => {
                    resolve(value)
                }, error => {
                    reject(error)
                })

            }
        ))

    }

    listUserbyRule(rule: number): Observable<User[]> {
        return this.httpClient.get<User[]>(`${environment.api_url}users/rule/${rule}`).pipe(
            map(res => res['result']));
    }
}

