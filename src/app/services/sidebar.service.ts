import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {Router} from '@angular/router';
import {distinctUntilChanged} from 'rxjs/operators';
import {UserService} from './user.service';
import {User} from '../model/user';
import {LocalStorageServices} from './local-storage.services';
import {LOCAL_STORAGE} from '../model/LOCAL_STORAGE';

// Metadata
class RouteInfo {
    path: string;
    title: string;
    type: string;
    icontype: string;
    collapse?: string;
    children?: ChildrenItems[];
}

class ChildrenItems {
    path: string;
    title: string;
    ab: string;
    type?: string;
}

@Injectable()
export class SidebarService {
    public routesSubject = new BehaviorSubject<any[]>(null);
    public routes = this.routesSubject.asObservable().pipe(distinctUntilChanged());
    private user: User;
    localStorageService: LocalStorageServices;

    constructor(private router: Router, private userService: UserService) {
        this.localStorageService = new LocalStorageServices();
        const username_local = this.localStorageService.getValue(LOCAL_STORAGE.USER_NAME);
        if (username_local) {
            this.userService.getCurrentUser(username_local).subscribe(user => {
                this.user = user
            });
        }
    }

    useHRRoutes(): void {
        // Menu Items
        const dashboardRoutes: RouteInfo[] = [{
            path: '/admin/dashboard',
            title: 'Dashboard',
            type: 'link',
            icontype: 'dashboard'
        }, {
            path: '/admin/candidate',
            title: 'Manage Candidate',
            type: 'sub',
            icontype: 'people',
            collapse: 'candidate',
            children: [
                {path: 'import', title: 'Import Candidate', ab: 'IC'},
                {path: 'review', title: 'Review Profile', ab: 'RP'}
            ]
        }, {
            path: '/admin/job',
            title: 'Manage Job',
            type: 'sub',
            icontype: 'work',
            collapse: 'job',
            children: [
                {path: 'addjob', title: 'Add Job', ab: 'AJ'},
                {path: 'listjob', title: 'List Job', ab: 'LJ'}
            ]
        },
            /*            {
                        path: '/config',
                        title: 'Config and Question',
                        type: 'sub',
                        icontype: 'build',
                        collapse: 'config',
                        children: [
                            {path: 'config', title: 'Config Default', ab: 'B'},
                            {path: 'question', title: 'Question Default', ab: 'GS'}
                        ]
                    }, */
            {
                path: '/admin/offer',
                title: 'Offer',
                type: 'link',
                icontype: 'how_to_reg'
            }
        ];
        this.routesSubject.next(dashboardRoutes);
    }

    useInterviewRoutes(): void {
        const interviewRoutes: RouteInfo[] = [{
            path: '/admin/dashboard',
            title: 'Dashboard',
            type: 'link',
            icontype: 'dashboard'
        }, {
            path: '/admin/interviewer',
            title: 'Interviewer',
            type: 'link',
            icontype: 'hearing'
        }, {
            path: '/admin/question',
            title: 'Builder Question',
            type: 'link',
            icontype: 'hearing'
        }];
        this.routesSubject.next(interviewRoutes);
    }
}
