import {
    MatAutocompleteModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatDialogModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatStepperModule, MatFormFieldModule,
} from '@angular/material';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {CommonModule} from '@angular/common';

@NgModule({
    exports: [
        MatAutocompleteModule,
        MatButtonModule,
        MatButtonToggleModule,
        MatCardModule,
        MatCheckboxModule,
        MatChipsModule,
        MatStepperModule,
        MatDatepickerModule,
        MatDialogModule,
        MatExpansionModule,
        MatGridListModule,
        MatIconModule,
        MatInputModule,
        MatListModule,
        MatMenuModule,
        MatNativeDateModule,
        MatPaginatorModule,
        MatProgressBarModule,
        MatProgressSpinnerModule,
        MatRadioModule,
        MatRippleModule,
        MatSelectModule,
        MatSidenavModule,
        MatSliderModule,
        MatSlideToggleModule,
        MatSnackBarModule,
        MatSortModule,
        MatTableModule,
        MatTabsModule,
        MatToolbarModule,
        MatTooltipModule,
        MatFormFieldModule

    ],
    declarations: []
})
export class MaterialModule {
}

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';

import {AppRoutingModule} from './app.routing';
import {ComponentsModule} from './components/components.module';

import {AppComponent} from './app.component';
import {AdminLayoutComponent} from './layouts/admin-layout/admin-layout.component';
import {TokenInterceptor} from './services/interceptor';
import {ApiService} from './services/api.service';
import {JobcandidateService} from './services/jobcandidate.service';
import {CandidateService} from './services/candidate.service';
import {AmazingTimePickerModule} from 'amazing-time-picker';
import {JobService} from './services/job.service';
import {InterviewCandidateService} from './services/interviewCandidate.service';
import {UserService} from './services/user.service';
import {SidebarService} from './services/sidebar.service';
import {NotFoundComponent} from './not-found-component/not-found.component';
import {UserProfileComponent} from './user-profile/user-profile.component';


@NgModule({
    imports: [
        CommonModule,
        BrowserAnimationsModule,
        FormsModule,
        MaterialModule,
        MatNativeDateModule,
        ComponentsModule,
        RouterModule,
        MatDatepickerModule,
        AppRoutingModule,
        HttpClientModule,
        AmazingTimePickerModule,


    ],
    declarations: [
        AppComponent,
        AdminLayoutComponent,
        NotFoundComponent,
        UserProfileComponent

    ],
    providers: [
        ApiService,
        JobcandidateService,
        CandidateService,
        JobService,
        InterviewCandidateService,
        UserService,
        SidebarService,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: TokenInterceptor,
            multi: true
        }
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
