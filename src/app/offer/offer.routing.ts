import { Routes } from '@angular/router';
import {OfferComponent} from './offer.component';


export const OfferRouter: Routes = [

    {
        path: '',
        component: OfferComponent
    }
];
