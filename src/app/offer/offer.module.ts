import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OfferComponent } from './offer.component';
import {RouterModule} from '@angular/router';
import {OfferRouter} from './offer.routing';

@NgModule({
  imports: [
    CommonModule,
      RouterModule.forChild(OfferRouter)
  ],
  declarations: [OfferComponent]
})
export class OfferModule { }
