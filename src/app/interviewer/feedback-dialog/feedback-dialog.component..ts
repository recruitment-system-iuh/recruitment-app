import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {JobcandidateService} from '../../services/jobcandidate.service';
import {DataFeedback} from '../../model/dataFeedback';
import {DomSanitizer} from '@angular/platform-browser';
import Swal from "sweetalert2";
declare const $: any;
@Component({
  selector: 'app-interview-detail',
  templateUrl: './feedback-dialog.component.html',
  styleUrls: ['./feedback-dialog.component..scss']
})
export class FeedbackDialogComponent implements OnInit {

  dataFeedback: DataFeedback;
  renderAction;
  constructor(public dialogRef: MatDialogRef<FeedbackDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private jobcandidateService: JobcandidateService,
              private sanitizer: DomSanitizer) {
    console.log('data');
    console.log(this.data.jobcanID);
    this.sanitizer = sanitizer;
  }

  ngOnInit(): void {
    const formRender = $('#render-wrap1');
    this.jobcandidateService.getDataFeedback(this.data.jobcanID).subscribe( data => {
      console.log('data feedback');
      console.log(data);
      this.dataFeedback = data;
      this.formRender(formRender, data.questionInterview);
    })
  }
  formRender(formRender, questionInterivew) {
    const formData = questionInterivew;
    this.renderAction = $('form', formRender).formRender({formData: formData, dataType: 'json'});
  }
  falseInterview() {
    const formData = this.renderAction.userData;
    const result = {
      'status': 0,
      'formData': formData,
      'feedback': ''
    }
    Swal.fire({
      title: 'Please fill feedback sumary ?',
      input: 'text',
      showCancelButton: true,
      type: 'question',
    }).then( value => {
      if (value.value) {
        result.feedback = value.value
        console.log(value.value);
        this.dialogRef.close(result);
      }
    });
  }
  passInterview() {
    const formData = this.renderAction.userData;
    const result = {
      'status': 1,
      'formData': formData,
      'feedback': ''
    }
    Swal.fire({
      title: 'Please fill feedback sumary ?',
      input: 'text',
      showCancelButton: true,
      type: 'question',
    }).then( value => {
      if (value.value) {
        result.feedback = value.value
        console.log(value.value);
        this.dialogRef.close(result);
      }

    });
  }
  getURLCV() {
    if (this.dataFeedback !== null) {
      // @ts-ignore
      return this.sanitizer.bypassSecurityTrustResourceUrl(this.dataFeedback.urlCV);
    }
  }

  offerInterview() {
    const formData = this.renderAction.userData;
    const result = {
      'status': 2,
      'formData': formData,
      'feedback': ''
    }
    Swal.fire({
      title: 'Please fill feedback sumary ?',
      input: 'text',
      showCancelButton: true,
      type: 'question',
    }).then( value => {
      if (value.value) {
        result.feedback = value.value
        console.log(value.value);
        this.dialogRef.close(result);
      }
    });
  }
}

