import { Routes } from '@angular/router';
import {InterviewerComponent} from './interviewer.component';


export const InterviewerRouter: Routes = [

    {
        path: '',
        component: InterviewerComponent
    }
];
