import {Component, OnInit} from '@angular/core';
import {JobService} from '../services/job.service';
import {JobcandidateService} from '../services/jobcandidate.service';
import {InterviewJob} from '../model/interviewJob';
import {MatDialog} from '@angular/material';
import {FeedbackDialogComponent} from './feedback-dialog/feedback-dialog.component.';
import {LOCAL_STORAGE} from '../model/LOCAL_STORAGE';
import swal from 'sweetalert2';

declare const $: any;
const formData1 = [
    {
        type: 'header',
        subtype: 'h1',
        label: 'Create Question'
    },
    {
        type: 'paragraph',
        label:
            'Interviewer create question.'
    }
];

@Component({
    selector: 'app-interviewer',
    templateUrl: './interviewer.component.html',
    styleUrls: ['./interviewer.component.scss']
})
export class InterviewerComponent implements OnInit {

    interviewJob: InterviewJob[];
    listInterJobCandi: InterviewJob[] = [];
    userID: string;

    constructor(public dialog: MatDialog,
                private jobService: JobService
        , private jobcandService: JobcandidateService) {
    }

    ngOnInit() {
        // this.builderQuestion();
        this.userID = window.localStorage.getItem(LOCAL_STORAGE.USER_ID);
        this.jobService.getJobByUserID(Number(this.userID), 0).subscribe(jobs => {
            console.log('jobs');
            console.log(jobs);
            /* Kiểm tra các Job trùng dữ liệu
            * Ví dụ [1,3,4,4,1,2] => [1,3,4,2]
            * */
            const result = jobs.filter((job, xIndex) => {
                const findResult = jobs.findIndex((fItem, fIndex) => job.job_id === fItem.job_id && fIndex < xIndex);
                return findResult === -1;
            });
            this.interviewJob = result.sort((a: InterviewJob, b: InterviewJob) => Number(a.job_id) - Number(b.job_id));
        });
        this.jobService.getJobByUserID(Number(this.userID), 1).subscribe(data => {
            console.log(data);
            this.listInterJobCandi = data;
        })
    }

    builderQuestion() {
        const formBuilder = $('#build-wrap');
        const formRender = $('#render-wrap');
        const options = {
            formData1,
//      prepend: '<h1>Profile for Miss Marple.</h1>', // DOM Object, Array of Dom Objects/Strings or String
//      append: '<h2>All information is confidential.</h2>',
            disabledActionButtons: ['data'],
            //    editOnAdd: true, // show giao dien edit lun
            onSave: function (evt, formData) {
                formBuilder.toggle();
                formRender.toggle();
                console.log(formData);
                $('form', formRender).formRender({formData});
                const getUserDataBtn = document.getElementById('feedback-data');
                getUserDataBtn.addEventListener(
                    'click',
                    () => {
                        //      window.alert(JSON.stringify(formRender.formRender('userData')));
                        saveFeddbackData(formRender.formRender('userData'));
                    },
                    false
                );

                function saveFeddbackData(data) {
                    console.log(data);
                }
            }
        };
        formBuilder.formBuilder(options);
        $('.edit-form', formRender).click(function () {
            formBuilder.toggle();
            formRender.toggle();
        });
    }

    feedbackDialog(jobcanID) {
        console.log(jobcanID);
        const data = [];
        this.jobcandService.getDataFeedback(jobcanID).subscribe(respone => {
            console.log('data feedback');
            console.log(data);
            // Kiểm tra permission khi click button Feedback
            // load Diaglog feedback
            if (respone.questionInterview !== '') {
                const dialogRef = this.dialog.open(FeedbackDialogComponent,
                    {
                        disableClose: true,
                        data: {jobcanID: jobcanID},
                        height: '90%',
                        width: '95%',
                        panelClass: 'feedback-dialog'
                    });
                dialogRef.afterClosed().subscribe(result => {
                    // console.log(`Dialog result: ${result}`);
                    console.log(result)
                    console.log(jobcanID)
                    if (result !== '') {
                        const userData = result.formData.map(function (item) {
                            return item['userData'];
                        });
                        console.log(userData)
                        const body = {
                            'status': result.status,
                            'feedback': result.feedback,
                            'answerInterview': JSON.stringify(userData)
                        }
                        console.log(body);
                        if (result.status === 0) {
                            // false review
                            swal.fire({
                                type: 'warning',
                                title: 'False Interview!...'
                            })
                        } else {
                            if (result.status === 1) {
                                // Pass review
                                console.log(result.formData);
                                this.jobcandService.updateFeedback(jobcanID, body).subscribe(response => {
                                    console.log(response);
                                    swal.fire({
                                        type: 'success',
                                        title: 'Pass Interview Round!...'
                                    })
                                    window.location.reload();
                                })
                            } else {
                                // Offer
                                this.jobcandService.updateFeedback(jobcanID, body).subscribe(response => {
                                    console.log(response);
                                    swal.fire({
                                        type: 'success',
                                        title: 'Pass reivew!...'
                                    })
                                    window.location.reload();
                                })
                            }
                        }
                    } else {
                        swal.fire({
                            type: 'warning',
                            title: 'Cancel!...',
                            text: 'You clicked the button!',
                        })
                    }
                });
            } else {
                swal.fire({
                    type: 'error',
                    title: 'Not found Interview Question...',
                    text: 'Please add question Interview at tab Builder Question ',
                })
            }
        })

    }

    getJobCandiDetail(jobid) {
        this.jobService.getJobByUserID(Number(this.userID), jobid).subscribe(data => {
            console.log(data);
            this.listInterJobCandi = data;
        })
    }

}

