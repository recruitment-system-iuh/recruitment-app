import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InterviewerComponent } from './interviewer.component';
import {RouterModule} from '@angular/router';
import {InterviewerRouter} from './interviewer.routing';
import {FeedbackDialogComponent} from './feedback-dialog/feedback-dialog.component.';
import {MaterialModule} from '../app.module';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(InterviewerRouter),
    MaterialModule
  ],
  declarations: [InterviewerComponent, FeedbackDialogComponent],
  entryComponents: [FeedbackDialogComponent]
})
export class InterviewerModule { }
