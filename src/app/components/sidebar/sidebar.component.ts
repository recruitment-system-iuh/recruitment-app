import {Component, OnInit} from '@angular/core';
import PerfectScrollbar from 'perfect-scrollbar';
import {UserService} from '../../services/user.service';
import {SidebarService} from '../../services/sidebar.service';
import {Router} from '@angular/router';
import {User} from '../../model/user';
import {LocalStorageServices} from '../../services/local-storage.services';
import {LOCAL_STORAGE} from '../../model/LOCAL_STORAGE';

declare const $: any;


@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
    public menuItems: any[];
    public user: User;
    localStorageService: LocalStorageServices;

    constructor(private userService: UserService,
                private sidebarService: SidebarService,
                private router: Router,
    ) {
        this.localStorageService = new LocalStorageServices();


    }

    ngOnInit() {
        this.sidebarService.routes.subscribe(routes => this.menuItems = routes && routes.filter(menuItem => menuItem));
        const userName = this.localStorageService.getValue(LOCAL_STORAGE.USER_NAME);
        if (userName) {
            this.userService.getCurrentUser(userName).subscribe(user => {
                this.user = user
            });
        }
    }

    isMobileMenu() {
        if ($(window).width() > 991) {
            return false;
        }
        return true;
    };

    updatePS(): void {
        if (window.matchMedia(`(min-width: 960px)`).matches && !this.isMac()) {
            const elemSidebar = <HTMLElement>document.querySelector('.sidebar .sidebar-wrapper');
            const ps = new PerfectScrollbar(elemSidebar, {wheelSpeed: 2, suppressScrollX: true});
        }
    }

    isMac(): boolean {
        let bool = false;
        if (navigator.platform.toUpperCase().indexOf('MAC') >= 0 || navigator.platform.toUpperCase().indexOf('IPAD') >= 0) {
            bool = true;
        }
        return bool;
    }
}
