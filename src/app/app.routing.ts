import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {BrowserModule} from '@angular/platform-browser';
import {Routes, RouterModule} from '@angular/router';
import {AdminLayoutComponent} from './layouts/admin-layout/admin-layout.component';
import {NotFoundComponent} from './not-found-component/not-found.component';

const routes: Routes = [
    {
        path: '',
        redirectTo: 'client',
        pathMatch: 'full',
    }, {
        path: 'admin',
        component: AdminLayoutComponent,
        children: [
            {
                path: 'dashboard',
                loadChildren: './dashboard/dashboard.module#DashboardModule'
            }, {
                path: 'candidate',
                loadChildren: './manage-candidate/manage-candidate.module#ManageCandidateModule'
            }, {
                path: 'job',
                loadChildren: './manage-job/manage-job.module#ManageJobModule'
            }, {
                path: 'offer',
                loadChildren: './offer/offer.module#OfferModule'
            }, {
                path: 'interviewer',
                loadChildren: './interviewer/interviewer.module#InterviewerModule'
            }, {
                path: 'question',
                loadChildren: './config-question/config-question.module#ConfigQuestionModule'
            },

        ]
    }, {
        path: 'auth',
        children: [{
            path: '',
            loadChildren: './pages/pages.module#PagesModule'
        }]
    }, {

        path: 'client',
        children: [
            {
                path: '',
                loadChildren: './client-page/client-page.module#ClientPageModule'
            }
        ]


    }, {
        path: '**',
        component: NotFoundComponent
    }
];

@NgModule({
    imports: [
        CommonModule,
        BrowserModule,
        RouterModule.forRoot(routes)
    ],
    exports: [],
})
export class AppRoutingModule {
}
