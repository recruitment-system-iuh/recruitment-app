import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ListjobComponent} from './listjob/listjob.component';
import {AddJobComponent} from './add-job/add-job.component';
import {RouterModule} from '@angular/router';
import {JobRouter} from './manage-job.routing';
import {MaterialModule} from '../app.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AmazingTimePickerModule} from 'amazing-time-picker';
import { InterviewDetailComponent } from './listjob/interview-detail/interview-detail.component';
import {ScheduleDialogComponent} from './listjob/schedule-dialog/schedule-dialog.component';
import {ConfigtypeDialogComponent} from './add-job/configtype-dialog/configtype-dialog.component';


@NgModule({
    imports: [
        MaterialModule,
        CommonModule,
        RouterModule.forChild(JobRouter),
        FormsModule, ReactiveFormsModule,
        AmazingTimePickerModule
    ],
    declarations: [ListjobComponent, AddJobComponent,
        ScheduleDialogComponent, InterviewDetailComponent, ConfigtypeDialogComponent],
    entryComponents: [ScheduleDialogComponent, InterviewDetailComponent, ConfigtypeDialogComponent]

})
export class ManageJobModule {

}
