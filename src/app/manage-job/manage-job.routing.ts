import {Routes} from '@angular/router';
import {AddJobComponent} from './add-job/add-job.component';
import {ListjobComponent} from './listjob/listjob.component';


export const JobRouter: Routes = [

    {
        path: '',
        children: [{
            path: 'listjob',
            component: ListjobComponent
        }, {
            path: 'addjob',
            component: AddJobComponent
        }]
    }
];
