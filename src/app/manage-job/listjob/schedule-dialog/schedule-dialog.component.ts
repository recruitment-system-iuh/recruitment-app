import {AfterViewInit, Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef, MatTableDataSource} from '@angular/material';
import {AmazingTimePickerService} from 'amazing-time-picker';
import swal from 'sweetalert2';
import {InterviewDetailComponent} from '../interview-detail/interview-detail.component';
import {SelectionModel} from '@angular/cdk/collections';
import {InterviewJobCandidate} from '../../../model/interviewJobCandidate';
import {UserService} from '../../../services/user.service';
import {User} from '../../../model/user';
import {Schedule} from '../../../model/MODEL';
import {MAT_MOMENT_DATE_FORMATS, MomentDateAdapter} from '@angular/material-moment-adapter';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import {InterviewCandidateService} from '../../../services/interviewCandidate.service';
@Component({
    selector: 'app-schedule-dialog',
    templateUrl: './schedule-dialog.component.html',
    styleUrls: ['./schedule-dialog.component.scss'],
    providers: [
        // `MomentDateAdapter` and `MAT_MOMENT_DATE_FORMATS` can be automatically provided by importing
        // `MatMomentDateModule` in your applications root module. We provide it at the component level
        // here, due to limitations of our example generation script.
        {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
        {provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS},
    ]
})
export class ScheduleDialogComponent implements OnInit, AfterViewInit {

    scheduleForm: FormGroup;
    listInterview: User[];
    listSupporter: User[];
    timeSchedule = '19:00';

    dataSource: MatTableDataSource<InterviewJobCandidate>;
    displayedColumns: string[] = ['select', 'name', 'email', 'phone'];
    selection = new SelectionModel<InterviewJobCandidate>(true, []);

    constructor(public dialogRef: MatDialogRef<InterviewDetailComponent>,
                private atp: AmazingTimePickerService,
                @Inject(MAT_DIALOG_DATA) public data: InterviewJobCandidate[],
                private userService: UserService,
                private formBuilder: FormBuilder,
                private scheduleService: InterviewCandidateService
                ) {
    }

    ngOnInit(): void {
        this.scheduleForm = this.formBuilder.group({
            address: ['', Validators.compose([Validators.required])],
            time: [''],
            date: ['', Validators.required],
            interviewer: ['', Validators.required],
            supporter: ['']
        });
        // console.log('data');
        // console.log(this.data);
        this.dataSource = new MatTableDataSource<InterviewJobCandidate>(this.data);

    }

    ngAfterViewInit() {
        this.userService.listUserbyRule(1).subscribe(user => {
            this.listInterview = user;
        })
        // Sử dụng để fix lỗi popup candidate hiển thị phía sau component dialog
        const x = <HTMLElement>document.getElementsByClassName('cdk-global-overlay-wrapper')[0];
        x.style.zIndex = '1000';

    }

/*    initTime() {
        const date = new Date();
        this.timeSchedule =  moment(date).format('HH:mm:ss');
    }*/

    open() {
      //  this.initTime()
        console.log(this.timeSchedule);
        const amazingTimePicker = this.atp.open({time: this.timeSchedule});
        const x = <HTMLElement>document.getElementById('time-picker-wrapper');
        x.style.zIndex = '9999';
        amazingTimePicker.afterClose().subscribe(time => {
            this.timeSchedule = time;
        });
    }

    close() {
        this.dialogRef.close();
    }

    isAllSelected() {
        const numSelected = this.selection.selected.length;
        const numRows = this.dataSource.data.length;
        return numSelected === numRows;
    }

    /** Selects all rows if they are not all selected; otherwise clear selection. */
    masterToggle() {
        this.isAllSelected() ?
            this.selection.clear() :
            this.dataSource.data.forEach(row => this.selection.select(row));
    }

    validateFromSchedule() {
        if (this.scheduleForm.invalid) {
            return false;
        }
        if (this.timeSchedule === '') {
            swal.fire({
                type: 'error',
                title: 'Bad!...',
                text: 'Please select time!',
            })
            return false;
        }
        if (this.selection.selected.length < 1) {
            swal.fire({
                type: 'error',
                title: 'Bad!...',
                text: 'Please select candidate!',
            })
            return false;
        }
        return true;
    }

    creatSchedule() {
        if (!this.validateFromSchedule()) {
            return;
        }
        const listjobCandID = [];
        for (let i = 0; i < this.selection.selected.length; i++) {
            listjobCandID.push(this.selection.selected[i].jobcanID + '');
        }
/*        console.log(this.scheduleForm.controls);
        console.log(listjobCandID);*/
        const formData = this.scheduleForm.controls.date.value._i;
        if (formData.date < 10) {
            formData.date = '0' +  formData.date;
        }
        if (formData.month < 10) {
            formData.month = '0' + `${formData.month + 1}`;
        } else {
            formData.month = formData.month + 1;
        }
        const date = formData.date + '-' + formData.month + '-' + formData.year;
        const schedule: Schedule = {
            time: date + ' ' + this.timeSchedule,
            address: this.scheduleForm.controls.address.value,
            interviewer: this.scheduleForm.controls.interviewer.value.id,
            supporters: this.scheduleForm.controls.supporter.value,
            jobcandIDs: listjobCandID
        }
        console.log(schedule);
        this.scheduleService.createSchedule(schedule).subscribe( (res) => {
            swal.fire({
                type: 'success',
                title: 'Good job!...',
                text: 'You clicked the button!',
            })
            window.location.reload();
            this.dialogRef.close();
        })

    }

    selectInterviewer(event) {
        if (event.isUserInput) {
            this.listSupporter = [];
            for (let i = 0; i < this.listInterview.length; i++) {
                const user = this.listInterview[i];
                if (user.id !== event.source.value.id) {
                    this.listSupporter.push(user);
                }
            }
        }
    }
}
