import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatDialogConfig, MatPaginator, MatTableDataSource} from '@angular/material';
import {JobService} from '../../services/job.service';
import {JobcandidateService} from '../../services/jobcandidate.service';
import {InterviewCandidateService} from '../../services/interviewCandidate.service';
import {DataSource, SelectionModel} from '@angular/cdk/collections';
import {ScheduleDialogComponent} from './schedule-dialog/schedule-dialog.component';
import {InterviewDetailComponent} from './interview-detail/interview-detail.component';
import {BehaviorSubject, Observable} from 'rxjs';
import {InterviewJobCandidate} from 'app/model/interviewJobCandidate';
import {Job} from '../../model/job';
import {ScheduleInterviewDetail} from '../../model/scheduleInterviewDetail';

@Component({
    selector: 'app-listjob',
    templateUrl: './listjob.component.html',
    styleUrls: ['./listjob.component.scss']
})
export class ListjobComponent implements OnInit, AfterViewInit {
    @ViewChild(MatPaginator) paginator: MatPaginator;
    displayedColumns: string[] = ['name', 'email', 'phoneNumber', 'dateApply', 'round', 'status', 'action'];
    dataSource;
    listJob: Job[] = [];
    constructor(public dialog: MatDialog, private jobService: JobService,
                private jobcandiService: JobcandidateService,
                private interviewCandidateService: InterviewCandidateService) {
    }

    ngOnInit() {
        this.dataSource = new ListInterviewCandiDataSource(this.jobcandiService);
        this.dataSource.getJobDetailAndCandidateInterview('1');
    }

    ngAfterViewInit(): void {
        this.jobService.getlistjob().subscribe(data => {
            // @ts-ignore
            this.listJob = data;
         //   console.log(this.listJob);
        })
    }
    getJobDetail(job) {
        this.dataSource.getJobDetailAndCandidateInterview(job.id);
    }
    async detailInterviewDialog(jobcanID) {
        console.log(jobcanID);
        // @ts-ignore
        const data: ScheduleInterviewDetail[] = await this.getScheduleDetail(jobcanID);
        const dialogRef = this.dialog.open(InterviewDetailComponent, {disableClose: true, data});
        dialogRef.afterClosed().subscribe(result => {
            // console.log(`Dialog result: ${result}`);
            console.log(result)
        });

    }

    getScheduleDetail = (jobcanID) => {
        return new Promise((resolve, reject) => {
            this.interviewCandidateService.getScheduleInterviewDetailbyJobcanID(jobcanID).subscribe(data => {
                resolve(data);
            })
        })
    }
    async scheduleActionButton() {
        const data = await this.getJobCandiWithStatusWaiting();
        console.log(data);
        const dialogCfg = new MatDialogConfig();
        dialogCfg.autoFocus = true;
        dialogCfg.disableClose = true;
        dialogCfg.data = data;
        const dialogSchedule = this.dialog.open(ScheduleDialogComponent, dialogCfg);
        dialogSchedule.afterClosed().subscribe(result => {
            // console.log(`Dialog result: ${result}`);
           // console.log(result)
            this.dataSource.getJobDetailAndCandidateInterview(data[0].id);
        });
    }
    getJobCandiWithStatusWaiting() {
        return new Promise((resolve, reject) => {
            const list: InterviewJobCandidate[] = this.dataSource.interviewCandiSubject.value;
            const listResult: InterviewJobCandidate[] = [];
            for (let i = 0 ; i < list.length; i++) {
                const interview: InterviewJobCandidate = list[i];
                if (interview.statusInterview === 'waiting') {
                    listResult.push(interview);
                }
            }
            resolve(listResult);
        })
    }

}

export class ListInterviewCandiDataSource extends DataSource<InterviewJobCandidate> {
    private interviewCandiSubject = new BehaviorSubject<InterviewJobCandidate[]>([]);
    private loadingSubject = new BehaviorSubject<boolean>(false);
    public loading$ = this.loadingSubject.asObservable();

    constructor(private jobcandidateService: JobcandidateService) {
        super();
    }

    getJobDetailAndCandidateInterview(jobid) {
        this.loadingSubject.next(true);
        this.jobcandidateService.getJobDetailAndCandiInterview(jobid).subscribe(data => this.interviewCandiSubject.next(data));
    }

    connect(): Observable<InterviewJobCandidate[]> {
        return this.interviewCandiSubject.asObservable();
    }

    disconnect() {
        this.interviewCandiSubject.complete();
        this.loadingSubject.complete()
    };
}
