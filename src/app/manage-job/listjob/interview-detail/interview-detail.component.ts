import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {ScheduleInterviewDetail} from '../../../model/scheduleInterviewDetail';

@Component({
  selector: 'app-interview-detail',
  templateUrl: './interview-detail.component.html',
  styleUrls: ['./interview-detail.component.scss']
})
export class InterviewDetailComponent {

  displayedColumns: String [] = ['round', 'time', 'address', 'status', 'feedback', 'interviewer'];
  constructor(public dialogRef: MatDialogRef<InterviewDetailComponent>,
              @Inject(MAT_DIALOG_DATA) public data: ScheduleInterviewDetail[]) {
    console.log('data');
    console.log(this.data);
  }
}
