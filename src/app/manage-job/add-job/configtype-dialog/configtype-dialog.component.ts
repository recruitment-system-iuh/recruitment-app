import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';
import * as _ from 'lodash'

@Component({
  selector: 'app-configtype-dialog',
  templateUrl: './configtype-dialog.component.html',
  styleUrls: ['./configtype-dialog.component.scss']
})
export class ConfigtypeDialogComponent implements OnInit {

  displayedColumns: String [] = ['select', 'configTypeName'];
  dataSource: MatTableDataSource<any>;
  selection = new SelectionModel<any>(true, []);
  constructor(public dialogRef: MatDialogRef<ConfigtypeDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) {
    console.log('data');
    console.log(this.data);
  }
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataSource.data.forEach(row => this.selection.select(row));
  }

  ngOnInit(): void {
    this.dataSource = new MatTableDataSource<any>(this.data.configType);
    this.dataSource.data.forEach((row) => {
      const data = _.find(this.data.selectedType, ['id', row.id])
      if ( data !== undefined ) {
        this.selection.select(row);
      }
    })
  }
  selectConfigType() {
    const listConfigType = [];
    for (let i = 0; i < this.selection.selected.length; i++) {
      listConfigType.push(this.selection.selected[i]);
    }
    this.dialogRef.close(listConfigType);
  }
}
