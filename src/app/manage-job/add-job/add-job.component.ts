import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ApiService} from '../../services/api.service';
import {JobService} from '../../services/job.service';
import swal from 'sweetalert2';
import {MatDialog} from '@angular/material';
import {ConfigtypeDialogComponent} from './configtype-dialog/configtype-dialog.component';
import * as _ from 'lodash'
@Component({
    selector: 'app-add-job',
    templateUrl: './add-job.component.html',
    styleUrls: ['./add-job.component.scss']
})
export class AddJobComponent implements OnInit {
    addJobForm: FormGroup;
    listConfigType: Array<{
        id: number,
        name: string
        configs: Array<{
            id: number,
            name: string,
            content: object
        }>,
        configsSelected: Array<any>,
        model: object

    }>;
    listSelected: Array<any>;
    listConfig = [];
    constructor(private formBuilder: FormBuilder,
                private apiServices: ApiService,
                private jobService: JobService,
                public dialog: MatDialog) {

    }
    ngOnInit() {
        this.listConfigType = [];
        this.listSelected = [];
        this.apiServices.getConfigTypes().then(data => {
            console.log(data);
            data.result.forEach(type => {
                const configs = [];
                type['configs'].forEach(conf => {
                    configs.push({
                        id: conf['id'],
                        name: conf['name'],
                        content: []
                    })
                });
                this.listConfigType.push({
                    id: type['id'],
                    name: type['name'],
                    configs: configs,
                    configsSelected: [],
                    model: JSON.parse(type['content'])
                });

            });
        }, error => {
            console.log(error);
        });

        this.addJobForm = this.formBuilder.group({
            jobName: ['', Validators.required],
            position: ['', Validators.required],
            numberRecruit: ['', Validators.required],
            roundInterview: ['', Validators.required],
            jobDes: ['', Validators.required],
            jobRequire: ['', Validators.required],
            jobSelect: [[]],
         //   jobSelect: [[], Validators.required],
        });
    }

    onSubmit(fr: FormGroup) {
        const job = this.generateJobJSON(fr);
        // this.apiServices.saveNewJob(job).then(mes => {
        //     console.log(mes);
        // }, err => {
        //     console.log(err);
        // });
        if (fr.invalid) {
            return;
        }
        console.log(job);
        this.jobService.saveNewJob(job).subscribe( respone => {
            console.log(respone);

            swal.fire({
                type: 'success',
                title: 'Oops...',
                text: 'Add Job Successful!',
            })
            window.location.reload();
        })

    }

    private generateJobJSON(fr: FormGroup) {
        const form = fr['form'];
        const job = {
            'jobDescription': 'aaaaaaaaaaaaaaa',
            'jobName': 'Java',
            'position': 'Senior',
            'round': 3,
            'numberRecruitment': 40,
            'jobRequirement': 'aaaa',
            'configs': [
                {
                    'id': 1,
                    'name': 'C#',
                    'content': '{}',
                    'configtype_id': 1
                },
                {
                    'id': 2,
                    'name': 'JAVA',
                    'content': '{}',
                    'configtype_id': 1
                },
                {
                    'id': 3,
                    'name': 'C++',
                    'content': '{}',
                    'configtype_id': 1
                },
                {
                    'id': 0,
                    'name': 'Outsource',
                    'content': '{}',
                    'configtype_id': 2
                }
            ]

        };
        job.jobDescription = form.value.jobDes;
        job.jobName = form.value.jobName;
        job.position = form.value.position;
        job.round = form.value.roundInterview;
        job.numberRecruitment = form.value.numberRecruit;
        job.jobRequirement = form.value.jobRequire;
        job.configs = this.processListConfig();
        console.log('job ne');
        console.log(job);
        return job;
    }

    private processListConfig() {
        const outputConfig = [];
        this.listSelected.forEach(configselected => {
            if (configselected.configsSelected.length > 0) {
                configselected.configsSelected.forEach(elm => {
                    if (elm.content.length > 0) {
                        const config = {
                            id: '',
                            name: '',
                            content: {},
                            configtype_id: ''
                        };
                        config.configtype_id = configselected.id;
                        config.id = elm.id;
                        config.name = elm.name;
                        const content = {};
                        let flag = 0;
                        configselected.model.configtypes.forEach(model_elm => {
                            model_elm.properties.forEach(prop_elm => {
                                content[prop_elm.property] = elm.content[flag];
                                flag++;
                            });
                        });
                        config.content = content;
                        outputConfig.push(config);
                    }
                });
            }
        });
        return outputConfig;
    }
    initialModel(i: number) {
        return true;
    }
    configTypeDialog() {
        const data = {
            'configType': this.listConfigType,
            'selectedType': this.listSelected,
        };
        const dialogRef = this.dialog.open(ConfigtypeDialogComponent, {
            disableClose: true,
            data,
            height: '55%',
            width: '50%',
        });
        dialogRef.afterClosed().subscribe(result => {
            // console.log(`Dialog result: ${result}`);
            console.log(result)
            this.listSelected = result;
        });

    }
    removeConfigType(configTypeID) {
/*        const data = _.find(this.listSelected, ['id', configTypeID])
        console.log(data);
        this.listConfig.push();*/
        this.listSelected = _.remove(this.listSelected, item => item.id !== configTypeID);
    }

}
