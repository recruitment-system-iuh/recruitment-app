export interface InterviewJobCandidate {
    id: string;
    jobcanID: string;
    jobname: string;
    position: string;
    jobRound: string;
    numberRecruitment: number;
    candidateName: string;
    email: string;
    phoneNumber: string;
    createdDate: string;
    interviewRound: number;
    statusInterview: string;
    jobcandiStatus: string;

}
