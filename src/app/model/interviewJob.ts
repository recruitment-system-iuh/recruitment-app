export interface InterviewJob {
    job_id: number;
    jobname: string;
    position: string;
    jobRound: number;
    name: string;
    email: string;
    jobcanID: number;
    createdAt: string;
    jvRound: number;
    jci_id: number;
};
