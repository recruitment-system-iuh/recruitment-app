export interface DataReview {
     jobName: String;
     jobDescription: String;
     position: String;
     jobRequirement: String;
     resumeDetais: [{
          header: String;
          subHeader: String;
          content: [];
     }];
     jobConfigs: [{
          content: [];
          configTypeID: number;
          configName: String;
          configTypeName: String;
     }]
}

