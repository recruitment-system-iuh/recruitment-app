export interface User {
    id: string;
    username: string;
    rule: number;
    person: {
        id: string;
        address: string;
        birthday: string;
        firstName: string;
        lastName: string;
        gender: string;
        email: string;
        phoneNumber: string;
    }
}


