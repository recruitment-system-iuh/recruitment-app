export interface JobCandi {
    id: string;
    currentRound: string;
    idInterviewRound: string;
    status: string;
    statusInterview: string;
    createDateTime: string;
    updateDateTime: string;
    urlCV: string;
    candidateID: string;
    jobID: string;
    position: string;
    candidateName: string;
    jobName: string;
}
