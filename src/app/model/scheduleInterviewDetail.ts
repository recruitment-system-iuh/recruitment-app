export interface ScheduleInterviewDetail {
    jobcanID: number;
    round: number;
    status: string;
    dateTime: string;
    feedback: string;
    address: string;
    userFeedbackName: string;
}
export interface JobcandidateInterview {
     id: number;
     round: number;
     status:  string ;
     feedback: string;
     dateTime:  string ;
     address: string;
     questionInterview:  string;
     answerInterview: string
}
