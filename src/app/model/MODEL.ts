import {User} from './user';

export class Job {
    id: String;
    createDateTime: String;
    jobDescription: String;
    jobName: String;
    numberRecruitment: number;
    position: String;
    round: number;
    updateDateTime: String;
}

export class JobConfig {
    id: String;
    name: String;
}

export class Schedule {
    time: String;
    address: String;
    supporters: [];
    interviewer: Number;
    jobcandIDs: any[];
}



