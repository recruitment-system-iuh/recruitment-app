export interface CandidateProfile {
    id: string;
    logs: [{
        createDateTime: string,
        user_log: {
            person: {
                firstName: string;
                lastName: string;
            }
        },
        logDetails: [
            {
                id: string;
                changeValue: string;
                toValue: string;
                updateField: string;
            }
            ]
    }]
    person: {
        id: string;
        address: string;
        birthday: string;
        firstName: string;
        lastName: string;
        gender: string;
        email: string;
        phoneNumber: string;
    }
}
