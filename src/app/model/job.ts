export interface Job {
    id: String;
    createDateTime: String;
    jobDescription: String;
    jobName: String;
    numberRecruitment: number;
    position: String;
    round: number;
    updateDateTime: String;
}
