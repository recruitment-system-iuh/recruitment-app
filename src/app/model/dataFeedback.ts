
export interface DataFeedback {
     jobName:  String;
     p_Recruitment:  String;
     name:   String;
     email:  String;
     p_apply:   String;
     questionInterview:  String;
     urlCV:  String ;
     round: number;
     address:  String ;
     date_Interview:  String;
}
