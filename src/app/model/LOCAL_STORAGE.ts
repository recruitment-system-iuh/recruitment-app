export const LOCAL_STORAGE = {
    TOKEN: 'token',
    USER_ID: 'USER_ID',
    USER_NAME: 'USER_NAME',
    JOB_ID:'JOB_ID',
    JOB_POSITION:'JOB_POSITION'
};
