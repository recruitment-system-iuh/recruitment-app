import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {DashboardComponent} from './dashboard.component';
import {RouterModule} from '@angular/router';
import {DashboardRoutes} from './dashboard.routing';
import { MdModule } from '../md/md.module';
import { FormsModule } from '@angular/forms';
@NgModule({
  imports: [
    CommonModule,
      RouterModule.forChild(DashboardRoutes),
      MdModule,
      FormsModule
  ],
  declarations: [DashboardComponent]
})
export class DashboardModule { }
