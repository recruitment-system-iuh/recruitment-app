import {Component, OnInit} from '@angular/core';
import {LocalStorageServices} from '../../services/local-storage.services';
import {Router} from '@angular/router';
import {User} from '../../model/user';
import {UserService} from '../../services/user.service';
import {LOCAL_STORAGE} from '../../model/LOCAL_STORAGE';

@Component({
    selector: 'app-client-profile-page',
    templateUrl: './client-profile.component.html',

})
export class ClientProfileComponent implements OnInit {
    skills: String [] = ['Java', 'C#'];
    currentSkill: String [] = [];
    localStorageService: LocalStorageServices;
    user_current: User;

    constructor(private router: Router, private userService: UserService) {
        this.localStorageService = new LocalStorageServices();
    }

    ngOnInit() {
        const user_name = this.localStorageService.getValue(LOCAL_STORAGE.USER_NAME);
        if (user_name) {
            this.userService.getCurrentUser(user_name).subscribe(data => {
                this.user_current = data;

            }, error => {
                console.log('Error', error);
                this.router.navigate(['client/home']);

            })
        } else {
            this.router.navigate(['client/home']);
        }


    }

    logoutButtonAction() {
        this.localStorageService.clearLocalStorage(LOCAL_STORAGE.USER_NAME);
        this.localStorageService.clearLocalStorage(LOCAL_STORAGE.USER_ID);
        this.localStorageService.clearLocalStorage(LOCAL_STORAGE.TOKEN);
        this.router.navigate(['client/home']);
    }
}

