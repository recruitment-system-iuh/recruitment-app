import {Component} from '@angular/core';
import {User} from '../../model/user';
import {LocalStorageServices} from '../../services/local-storage.services';
import {UserService} from '../../services/user.service';
import Swal from 'sweetalert2';
import {Router} from '@angular/router';
import {JobService} from '../../services/job.service';
import {LOCAL_STORAGE} from '../../model/LOCAL_STORAGE';
import {ApiService} from '../../services/api.service';

@Component({
    selector: 'app-client-home',
    templateUrl: './client-home.component.html',
    styleUrls: ['./client-home.component.scss']
})
export class ClientHomeComponent {

    user_current: User;
    localStorageService: LocalStorageServices;
    listJob = null;

    constructor(private userService: UserService, private router: Router, private jobService: JobService, private apiServices: ApiService) {
        this.localStorageService = new LocalStorageServices()

    }

    ngOnInit() {
        this.jobService.getlistjob().subscribe(data => {
            this.listJob = data;
        }, err => {
            console.log(err)
        })


    }

    ngAfterViewInit() {
        const user_name = this.localStorageService.getValue(LOCAL_STORAGE.USER_NAME);
        if (user_name) {
            this.userService.getCurrentUser(user_name).subscribe(data => {
                this.user_current = data;
            }, error => {
                console.log('Error', error);
            })
        }


    }

    checkExistsResume() {
        return false;
    }

    async onFileUpload(event) {
        // console.log(event.target);
        const files = event.target.files;
        await this.apiServices.saveFile(files[0], files[0].name).then(rs => {
            console.log(rs)
        }).catch(err => {
            console.log(err)
        });
        console.log('Done')

    }

    applyJobButtonAction(jobApply) {
        console.log(jobApply);

        if (this.user_current) {
            if (this.checkExistsResume()) {
                //Apply
            } else {
                //Create CV
                const swalWithBootstrapButtons = Swal.mixin({
                    customClass: {
                        confirmButton: 'btn btn-info',
                        // cancelButton: 'btn btn-success'

                    },
                    buttonsStyling: false,
                });

                swalWithBootstrapButtons.fire({
                    title: 'You dont have resume !',
                    type: 'warning',
                    confirmButtonText: 'Create resume now',
                    // showCancelButton: true,
                    cancelButtonText: 'Upload',
                    reverseButtons: true
                }).then((result) => {
                    if (result.value) {
                        //Click login
                        this.localStorageService.saveLocalStorage(LOCAL_STORAGE.JOB_ID, jobApply.id);
                        this.router.navigate(['client/gen-resume']);

                    } else if (result.dismiss === Swal.DismissReason.cancel) {
                        //Click register
                        document.getElementById('resumeUpload').click()
                        // this.router.navigate(['auth/register']);

                    }
                })
            }
        } else {
            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-success',
                    cancelButton: 'btn btn-info'
                },
                buttonsStyling: false,
            });

            swalWithBootstrapButtons.fire({
                title: 'You need login !',
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Login now',
                cancelButtonText: 'Register',
                reverseButtons: true
            }).then((result) => {
                if (result.value) {
                    //Click login
                    this.router.navigate(['auth/login-guest']);
                } else if (result.dismiss === Swal.DismissReason.cancel) {
                    //Click register
                    this.router.navigate(['auth/register']);

                }
            })


        }

    }

    logoutButtonAction() {
        this.localStorageService.clearLocalStorage(LOCAL_STORAGE.USER_NAME);
        this.localStorageService.clearLocalStorage(LOCAL_STORAGE.USER_ID);
        this.localStorageService.clearLocalStorage(LOCAL_STORAGE.TOKEN);
        this.router.navigate(['auth/']);
        window.location.reload()
    }
}

