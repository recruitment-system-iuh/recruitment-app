import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {RouterModule} from '@angular/router';

import {ClientPageRouter} from './client-page.routing';
import {ClientHomeComponent} from './client-home/client-home.component';
import {MaterialModule} from '../app.module';
import {FormsModule} from '@angular/forms';
import {ClientProfileComponent} from './client-profile/client-profile.component';
import {ClientInformationComponent} from './client-information/client-information.component';
import {ClientGenResumeComponent} from './client-gen-resume/client-gen-resume.component';
import {ResumeContentComponent} from './client-gen-resume/resume-content/resume-content.component';

@NgModule({
    imports: [
        CommonModule,
        MaterialModule,
        RouterModule.forChild(ClientPageRouter),
        FormsModule
    ],
    declarations: [ClientHomeComponent,
        ClientProfileComponent,
        ClientInformationComponent,
        ClientGenResumeComponent,
        ResumeContentComponent]
})
export class ClientPageModule {
}
