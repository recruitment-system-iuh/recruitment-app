import {Component, OnInit} from '@angular/core';
import {User} from '../../model/user';
import {LocalStorageServices} from '../../services/local-storage.services';
import {LOCAL_STORAGE} from '../../model/LOCAL_STORAGE';
import {UserService} from '../../services/user.service';
import {Router} from '@angular/router';

@Component({
    selector: 'app-client-gen-resume',
    templateUrl: './client-gen-resume.component.html'
})
export class ClientGenResumeComponent {
    user_current: User;
    localStorageService: LocalStorageServices;

    constructor(private userService: UserService, private router: Router,) {
        this.localStorageService = new LocalStorageServices()
    }

    ngAfterViewInit() {
        let user_name = this.localStorageService.getValue(LOCAL_STORAGE.USER_NAME);
        if (user_name) {
            this.userService.getCurrentUser(user_name).subscribe(data => {
                this.user_current = data;
            }, error => {
                console.log('Error', error);
            })
        }
    }

    logoutButtonAction() {
        this.localStorageService.clearLocalStorage(LOCAL_STORAGE.USER_NAME);
        this.localStorageService.clearLocalStorage(LOCAL_STORAGE.USER_ID);
        this.localStorageService.clearLocalStorage(LOCAL_STORAGE.TOKEN);
        this.router.navigate(['client/home']);
    }

}

