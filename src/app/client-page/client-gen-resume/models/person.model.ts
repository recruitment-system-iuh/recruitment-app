export class Person {
    firstName: string;
    lastName: string;
    jobTitle: string;
    address: string;
    phoneNumber: string;
    email: string;
    website: string;
    objective: string;
    birthday: String;
    gender : String;
    constructor() {

    }
}
