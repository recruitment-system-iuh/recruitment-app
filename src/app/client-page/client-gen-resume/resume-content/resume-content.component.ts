import {Component, OnInit, Inject, ViewChild, ElementRef} from '@angular/core';
import {Resume} from '../models/resume.model';
import {Education} from '../models/education.model';
import {School} from '../models/school.model';
import {Examination} from '../models/examination.model';
import {JobWork} from '../models/job-work.model';
import {Project} from '../models/project.model';
import Swal from 'sweetalert2';
import {JobService} from '../../../services/job.service';
import {LocalStorageServices} from '../../../services/local-storage.services';
import {LOCAL_STORAGE} from '../../../model/LOCAL_STORAGE';
import {CandidateService} from '../../../services/candidate.service';
import {UserService} from '../../../services/user.service';
import {User} from '../../../model/user';
import {Router} from '@angular/router';
import {ApiService} from '../../../services/api.service';
import {el} from '@angular/platform-browser/testing/src/browser_util';

// import * as jsPDF from 'jspdf'

@Component({
    selector: 'app-resume-gen-school-editor',
    templateUrl: './resume-content.component.html',
    styleUrls: ['./resume-content.component.css']
})
export class ResumeContentComponent implements OnInit {

    @ViewChild('profileView') content: ElementRef;

    resume: any;

    preview: Boolean;
    localStorageServices: LocalStorageServices;
    user: User;

    fileUpload = null;
    minBirthDay = '';

    constructor(private jobService: JobService,
                private candidateService: CandidateService,
                private router: Router,
                private userService: UserService,
                private apiServices: ApiService) {
        this.resume = new Resume();
        this.resume.person.gender = 'Male';
        this.preview = false;
        this.localStorageServices = new LocalStorageServices();
        let user_name = this.localStorageServices.getValue(LOCAL_STORAGE.USER_NAME);
        this.userService.getCurrentUser(user_name).subscribe(data => {
            this.user = data;
        }, error => {
            this.router.navigate(['auth/login-guest']);

        })
    }

    async onFileUpload(event) {
        this.fileUpload = event;
    }

    previewResume() {
        // console.log(this.resume)
        if (!this.resume.person.firstName || !this.resume.person.lastName
            || !this.resume.person.birthday || !this.resume.person.email) {
            Swal.fire({
                type: 'error',
                title: 'Oops...',
                text: 'Please full fill personal information!',
            });
            return;
        }
        if (!this.fileUpload) {
            Swal.fire({
                type: 'error',
                title: 'Oops...',
                text: 'Please attach resume!',
            });
            return;
        }
        if (!this.resume.education || !this.resume.education.schools
            || this.resume.education.schools.length < 1 || !this.resume.education.schools[0].name
            || this.resume.education.schools[0].length < 1) {
            Swal.fire({
                type: 'error',
                title: 'Oops...',
                text: 'Please add education history!',
            });
            return;
        }


        this.preview = true;
    }

    applyResume() {
        let jobID = this.localStorageServices.getValue(LOCAL_STORAGE.JOB_ID);
        let job = null;

        this.jobService.getJobByID(parseInt(jobID)).subscribe(data => {
            console.log(data);
            job = data;
            let position = job.position.split(',');
            let message = 'Job name : ' + job.jobName;
            if (position === undefined || position === null || position.length < 1) {
                Swal.fire({
                    type: 'error',
                    title: 'Oops...',
                    text: 'Job don\'t has position',
                });
                return;
            }
            Swal.fire({
                title: 'Do you want apply ?',
                text: message,
                input: 'select',
                inputOptions: position,
                inputValue: position[0],
                showCancelButton: true,
                type: 'question',

            }).then(async value => {
                let alert = Swal.fire({
                    imageUrl: 'assets/img/loading.svg',
                    showConfirmButton: false,
                    background: 'rgba(0, 0, 0, 0)'
                });
                let pos = position[parseInt(value.value.toString())].trim();

                let files = this.fileUpload.target.files;
                let filename = '';
                filename += this.user.username;
                filename += '-' + job.id + '-' + files[0].name;
                filename = filename.replace(/\s/g, '');
                let url = '';
                await this.apiServices.saveFile(files[0], filename).then(rs => {
                    url = rs.fileURL;
                }).catch(err => {
                    console.log(err)
                });
                console.log('Done');
                let job_apply = {
                    jobId: job.id,
                    position: pos,
                    resume: this.resume,
                    urlCV: url,
                    userId: this.user.id
                };
                console.log(job_apply);

                this.candidateService.applyJob(job_apply).subscribe(data => {
                    console.log(data);
                    Swal.fire({
                        position: 'center',
                        type: 'success',
                        title: 'Apply success!',
                        showConfirmButton: false,
                        timer: 1500
                    }).then(() => {
                        this.router.navigate(['client/home']);
                    })
                }, error => {
                    console.log(error)
                });


            });
        }, err => {
            console.log(err)
        });


    }

    // exportPdf() {
    //     let doc = new jsPDF();
    //     let specialElementHandlers = {
    //         '#editor': function (element, renderer) {
    //             return true;
    //         }
    //     };
    //     let content = this.content.nativeElement;
    //     doc.setFont('UVNBaiHoc_R');
    //
    //     doc.fromHTML(content.innerHTML, 15, 15, {
    //         'width': 190,
    //         'elementHandlers': specialElementHandlers
    //     });
    //     console.log(content.innerHTML)
    //
    //     doc.save('your_resume.pdf');
    // }

    backButton() {
        this.preview = false;
    }

    ngOnInit() {
        let date = new Date();
        let month = date.getMonth() + 1;
        let mth = '';
        if (month < 10) {
            mth = '0' + month;
        } else {
            mth = month + '';
        }
        let dateM = date.getDate();
        let dte = '';
        if (dateM < 10) {
            dte = '0' + dateM;
        } else {
            dte = dateM + '';
        }
        this.minBirthDay = (date.getFullYear() - 18) + '-' + mth + '-' + dte;

    }

    addSchool() {
        if (!this.resume.education) {
            this.resume.education = new Education();
        }

        if (!this.resume.education.schools) {
            this.resume.education.schools = [];
        }

        this.resume.education.schools.push(new School());

    }

    customTrackBy(index: number, obj: any): any {
        return index;
    }

    deleteSchool(index: number) {
        this.resume.education.schools.splice(index, 1);
    }

    addExamination() {
        if (!this.resume.education) {
            this.resume.education = new Education();
        }

        if (!this.resume.education.examinations) {
            this.resume.education.examinations = [];
        }

        this.resume.education.examinations.push(new Examination());

    }

    deleteExamination(index: number) {
        this.resume.education.examinations.splice(index, 1);
    }

    addAccomplishment() {
        if (!this.resume.accomplishments) {
            this.resume.accomplishments = [];
        } else {
            this.resume.accomplishments.push({value: ''});
        }
    }

    addJobWork() {
        if (!this.resume.jobWork) {
            this.resume.jobWork = [];
        }
        this.resume.jobWork.push(new JobWork());
    }

    deleteJobWork(index: any) {
        this.resume.jobWork.splice(index, 1);
    }

    addHighlight(jobWork) {
        if (!jobWork.highlights) {
            jobWork.highlights = [];
        }
        jobWork.highlights.push('');

    }

    addKeyword(jobWork) {
        if (!jobWork.keywords) {
            jobWork.keywords = [];
        }
        jobWork.keywords.push('');

    }

    addProject() {
        if (!this.resume.projects) {
            this.resume.projects = [];
        }
        this.resume.projects.push(new Project());
    }

    deleteProject(index: any) {
        this.resume.projects.splice(index, 1);
    }

    addOther() {
        if (!this.resume.others) {
            this.resume.others = [];
        }
        this.resume.others.push({key: '', value: ''});
    }

    addSkill() {
        if (!this.resume.skills) {
            this.resume.skills = [];
        }
        this.resume.skills.push({key: '', value: 0});
    }

    check() {
        console.log(this.resume.others.length);
        return false;
    }
}
