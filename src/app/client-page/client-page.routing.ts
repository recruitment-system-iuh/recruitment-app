import {Routes} from '@angular/router';
import {ClientHomeComponent} from './client-home/client-home.component';

import {ClientProfileComponent} from './client-profile/client-profile.component';
import {ClientInformationComponent} from './client-information/client-information.component';
import {ClientGenResumeComponent} from './client-gen-resume/client-gen-resume.component';

export const ClientPageRouter: Routes = [
    {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full',
    },
    {
        path: '',
        children: [{
            path: '',
            component: ClientHomeComponent
        }, {
            path: 'home',
            component: ClientHomeComponent
        }, {
            path: 'prof',
            component: ClientProfileComponent
        }, {
            path: 'inf',
            component: ClientInformationComponent
        }, {
            path: 'gen-resume',
            component: ClientGenResumeComponent
        }
        ]
    }
];
