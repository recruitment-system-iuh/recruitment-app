import { Routes } from '@angular/router';
import {ConfigComponent} from './config/config.component';
import {QuestionComponent} from './question/question.component';


export const ConfigQuestionRouter: Routes = [

    {
        path: '',
        component: QuestionComponent
    }
];
