import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ConfigComponent} from './config/config.component';
import {QuestionComponent} from './question/question.component';
import {RouterModule} from '@angular/router';
import {ConfigQuestionRouter} from './config-question.routing';
import {CreateQuestionDialogComponent} from './question/create-question-dialog/create-question-dialog.component.';
import {MaterialModule} from '../app.module';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ConfigQuestionRouter),
    MaterialModule
  ],
  declarations: [ConfigComponent, QuestionComponent, CreateQuestionDialogComponent],
  entryComponents: [CreateQuestionDialogComponent]
})
export class ConfigQuestionModule { }
