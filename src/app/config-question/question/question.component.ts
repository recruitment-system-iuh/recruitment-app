import {Component, OnInit} from '@angular/core';
import {JobService} from '../../services/job.service';
import {InterviewJob} from '../../model/interviewJob';
import {Job} from '../../model/job';
import {MatDialog} from '@angular/material';
import {CreateQuestionDialogComponent} from './create-question-dialog/create-question-dialog.component.';
import {InterviewJobCandidate} from '../../model/interviewJobCandidate';
import {InterviewCandidateService} from '../../services/interviewCandidate.service';
import {LOCAL_STORAGE} from '../../model/LOCAL_STORAGE';
import swal from 'sweetalert2';

class InterviewRound {
    roundNum: number;
    interviewRoundCandi: InterviewJob[];
}
@Component({
    selector: 'app-question',
    templateUrl: './question.component.html',
    styleUrls: ['./question.component.scss']
})
export class QuestionComponent implements OnInit {

    userID: string;
    interviewJob: InterviewJob[];
    detailJob: Job;
    interviewRounds: InterviewRound[] = [];
    constructor(private jobService: JobService,
                private dialog: MatDialog,
                private interviewCandidateService: InterviewCandidateService) {
    }

    ngOnInit() {
        this.userID = window.localStorage.getItem(LOCAL_STORAGE.USER_ID);
        this.jobService.getJobByUserID(Number(this.userID), 0).subscribe(jobs => {
            console.log('jobs');
            console.log(jobs);
            const result = jobs.filter((job, xIndex) => {
                const findResult = jobs.findIndex((fItem, fIndex) => job.job_id === fItem.job_id && fIndex < xIndex);
                return findResult === -1;
            });
            this.interviewJob =  result.sort((a: InterviewJob, b: InterviewJob) => Number(a.job_id) - Number(b.job_id));
        });
        this.jobService.getJobByID(1).subscribe( jobDetail => {
            this.detailJob = jobDetail;
            this.getJobCandiDetail(1, jobDetail.round);

        })
    }
    getJobCandiDetail(job_id, jobRound: number) {
        this.interviewRounds = [];
        this.jobService.getJobByID(job_id).subscribe( jobDetail => {
            this.detailJob = jobDetail;
        })
        this.jobService.getJobByUserID(Number(this.userID), job_id).subscribe(jobcands => {
            for (let i = 0; i < jobRound ; i++) {
                const interviewRound: InterviewRound = new InterviewRound ();
               // console.log(i);
                interviewRound.roundNum = i + 1;
                interviewRound.interviewRoundCandi = [];
                for ( let j = 0; j < jobcands.length ; j++) {
                    const jobcand: InterviewJob = jobcands[j];
                    /*
                    * Kiểm tra xem Vòng Phỏng Vấn của Candidate có bằng với Round không
                    * Nếu có add vào list phỏng vấn của vòng đó*/
                    if ( jobcand.jvRound === interviewRound.roundNum ) {
                        interviewRound.interviewRoundCandi.push(jobcand);
                    }
                }
                this.interviewRounds.push(interviewRound);
            }
        });
    }
    createQuestion(jobcands: InterviewJob[]) {
        console.log('create question')
        console.log(jobcands);
        const data = [];
        for ( let i = 0; i < jobcands.length ; i++) {
            data.push(jobcands[i].jci_id);
        }
        const dialogRef = this.dialog.open(CreateQuestionDialogComponent,
            {
                disableClose: true,
                data,
                height: '90%',
                width: '95%',
                panelClass: 'feedback-dialog'
            });
        dialogRef.afterClosed().subscribe(result => {
            // console.log(`Dialog result: ${result}`);
            // data form json
            console.log(result)
            if (result !== '') {
                for (let i = 0; i < result.jci_ids.length; i++ ) {
                    console.log(result.jci_ids[i]);
                    const id = result.jci_ids[i];
                    console.log(result.formData)
                    const body = {
                        'questionInterview': result.formData
                    }
                    console.log(body)
                    this.interviewCandidateService.updateQuestionJCI(id, result.formData).subscribe( response => {
                        console.log(response);
                        swal.fire({
                            type: 'success',
                            title: 'Save question success!...',
                            text: 'You clicked the button!',
                        })
                    })
                }

            } else {
                swal.fire({
                    type: 'warning',
                    title: 'Cancel!...',
                    text: 'You clicked the button!',
                })
            }

        });
    }

}
