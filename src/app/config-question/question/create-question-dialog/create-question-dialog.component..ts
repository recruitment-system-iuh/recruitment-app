import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {JobcandidateInterview} from '../../../model/scheduleInterviewDetail';
import {InterviewCandidateService} from '../../../services/interviewCandidate.service';
declare const $: any;

@Component({
  selector: 'app-create-question-dialog',
  templateUrl: './create-question-dialog.component.html',
  styleUrls: ['./create-question-dialog.component.scss']
})
export class CreateQuestionDialogComponent implements OnInit {

  displayedColumns: String [] = ['round', 'time', 'address', 'status', 'feedback', 'interviewer'];
  formBuilder;
  formRender;
  builderAction;
  jobcandiInterview: JobcandidateInterview;
  constructor(public dialogRef: MatDialogRef<CreateQuestionDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: [],
              private interviewCandidateService: InterviewCandidateService) {
    console.log('data');
    console.log(this.data);
  }
  ngOnInit(): void {
    this.formBuilder = $('#build-wrap');
    this.formRender = $('#render-wrap');
    // @ts-ignore
    this.interviewCandidateService.getJobCandidateInterviewByID(this.data[0]).subscribe( data => {
      this.jobcandiInterview = data;
      console.log(data)
      this.builderQuestion(this.formBuilder, this.formRender);
    })
  }
  builderQuestion(builder, render) {
    let data = this.jobcandiInterview.questionInterview;
    if ( data == null) {
      data = '';
    }
    console.log(data)
    const options = {
      formData: data,
      disabledActionButtons: ['data'],
      disabledAttrs: ['name', 'className'],
      controlPosition: 'left',
      showActionButtons: false
    };
    this.builderAction = builder.formBuilder(options);
    $('.edit-form', render).click(function() {
      builder.toggle();
      render.toggle();
      $('#dialog-end').toggle();
    });
  }
  reviewForm() {
    this.formBuilder.toggle();
    this.formRender.toggle();
    // tắt các nút review và save khi edit form
    $('#dialog-end').toggle();
    const formData = this.builderAction.actions.save();
    $('form', this.formRender).formRender({formData});
  }
  closeDialog() {
    const formData = this.builderAction.actions.save();
    const result = {
      'formData': formData,
      'jci_ids': this.data
    }
    this.dialogRef.close(result);
  }
}
