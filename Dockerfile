FROM node:10-alpine as builder

RUN mkdir /ng-app && cd /ng-app
WORKDIR /ng-app

RUN pwd
COPY package.json package-lock.json ./

## Storing node modules on a separate layer will prevent unnecessary npm installs at each build
RUN npm set progress=false && npm config set depth 0 && npm cache clean --force
RUN npm i

COPY . .

## Build the angular app in production mode and store the artifacts in dist folder

RUN npm run ng build -- --prod --aot --buildOptimizer --output-path=dist

FROM nginx:1.14.1-alpine

COPY nginx/nginx.conf /etc/nginx/

RUN rm -rf /usr/share/nginx/html/*

COPY --from=builder /ng-app/dist /usr/share/nginx/html

CMD ["nginx", "-g", "daemon off;"]
